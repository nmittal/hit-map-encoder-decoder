`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.08.2019 12:40:02
// Design Name: 
// Module Name: hit_map_encoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//This code encodes the Hit Data been generated by the Hit Generator before appending it to the stream encoder.
//Input: 77 bits --> 6 bits(core_col), 8 bits(qrow), 64 bits(hit data)
//Hit data is arranged in a 2x8 block
//             63:32
//             31:0       Height = 2 pixels        Width = 8 pixels   [1 pixel = 4bits]   
//The output of the module is (i) 44 bits of data --> 6 bits(core_col), 8 bits(qrow), 30 bits(encoded data)
//                           (ii) length of the encoded data 
//Encoded data is variable length ---> within those 30 bits, data is written starting from the MSB(29th position)
//Rest of the bits stays 0, if nothing gets written to them.

module hit_map_encoder(
                       input logic clk,
                       input logic reset,
                       input logic [77:0] in,
                       output logic [43:0] out,
                       output logic [4:0] length
                       );
logic [3:0] b [5:0];                     
logic [1:0] tree [14:0];
logic [5:0] a;
logic [29:0] z;
logic [4:0] len;
logic [29:0] encoded_data;

//generates the tree structure out of the hit data
binary_tree tree_gen(
                 .clk(clk),
                 .reset(reset),
                 .in(in[63:0]),
                 .tree(tree)
                  );

//serializes the data              
hit_data_stream encoded_stream(
                            .b(b),
                            .a(a),
                            .z(z),
                            .length(len)
                            );
                            
//1 bit encoding on the serialized data to reduce it further
bit_code_replacer bit_code(
                          .in(z),
                          .pre_replacer_length(len),
                          .out(encoded_data),
                          .post_replacer_length(length)
                         );
                              
integer i;
integer parent,left,right;

//Look up tables which looks at the nodes and eliminates the 2'b00
//if the node is empty and has no child ahead in the next level.
//So that output will not have 2 consecutive 0 in it. 
always_comb
  begin
    a = '0;
    case({tree[14],tree[13],tree[12]})
        6'b01_00_01: a = 4'b01_01;
        6'b01_00_10: a = 4'b01_10;
        6'b01_00_11: a = 4'b01_11;
        6'b10_11_00: a = 4'b10_11;
        6'b10_01_00: a = 4'b10_01;
        6'b10_10_00: a = 4'b10_10;
        6'b11_10_10: a = 6'b11_10_10;
        6'b11_01_10: a = 6'b11_01_10;
        6'b11_11_10: a = 6'b11_11_10;
        6'b11_10_01: a = 6'b11_10_01;
        6'b11_01_01: a = 6'b11_01_01;
        6'b11_11_01: a = 6'b11_11_01;
        6'b11_01_11: a = 6'b11_01_11;
        6'b11_10_11: a = 6'b11_10_11;
        6'b11_11_11: a = 6'b11_11_11;
    endcase
  end  
      

always_comb
  begin
    b[0] = '0; b[1] = '0; b[2] = '0; b[3] = '0; b[4] = '0; b[5] = '0; 
    parent = 13;
    left = 11;
    right = 10;
    for(i=0;i<6; i=i+1)
      begin: tree1
        case({tree[parent],tree[left],tree[right]})
          6'b01_00_01: b[i] = 2'b01;
          6'b01_00_10: b[i] = 2'b10;
          6'b01_00_11: b[i] = 2'b11;
          6'b10_11_00: b[i] = 2'b11;
          6'b10_01_00: b[i] = 2'b01;
          6'b10_10_00: b[i] = 2'b10;
          6'b11_10_10: b[i] = 4'b10_10;
          6'b11_01_10: b[i] = 4'b01_10;
          6'b11_11_10: b[i] = 4'b11_10;
          6'b11_10_01: b[i] = 4'b10_01;
          6'b11_01_01: b[i] = 4'b01_01;
          6'b11_11_01: b[i] = 4'b11_01;
          6'b11_01_11: b[i] = 4'b01_11;
          6'b11_10_11: b[i] = 4'b10_11;
          6'b11_11_11: b[i] = 4'b11_11;
        endcase
        parent = parent - 1;
        left = left - 2;
        right = right - 2;
      end 
  end  
  
logic [13:0] address_n, address_p;

assign address_n = in[77:64];
assign out = {address_p,encoded_data};

//storing the address for one clock cycle so that it can sychronize with the corresponding output data
always_ff @(posedge clk)
  begin
    if(reset)
      address_p <= 14'd0;
    else
      address_p <= address_n;
 end  
  
                
endmodule
