`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/16/2019 10:57:48 AM
// Design Name: 
// Module Name: binary_tree_n
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//                       TREE STRUCTURE                     
//                             14                      <- LEVEL 1  (root)
//                       /           \
//                     13             12               <- LEVEL 2
//                   /    \          /   \
//                 11      10      9      8            <- LEVEL 3
//               /   \    /  \   /  \   /  \
//              7    6   5   4  3    2  1   0          <- LEVEL 4
//
//This is the addressing of the tree used in the code below. 
//Each node is 2 bits each.    
//If the MSB = 1 --> There exist a left child of that node
//else left child == 2'b00
//same goes with LSB, which is for right child of that node.

//      |63:60|59:56|55:52|51:48|47:44|43:40|39:36|35:32|
//      |31:28|27:24|23:20|19:16|15:12|11:8 |7:4  |3:0  |
//First step: Divide vertically  (left-right)
//Second step: Divide horizontally  (top-bottom)
//Third step: Divide vertically  (left-right)
//Forth step: Divide vertically  (left-right) 

module binary_tree(
                  input logic clk,
                  input logic reset,
                  input logic [63:0] in,
                  output logic [1:0] tree [14:0]
                  );
                  
logic [1:0] tree_n [14:0];
logic [1:0] tree_p [14:0];

always_comb 
  begin
    //root 
    if((|in[63:48]) || (|in[31:16]))
      tree_n[14][1] = 1'b1;
    else
      tree_n[14][1] = 1'b0;   
    if((|in[47:32]) || (|in[15:0]))
      tree_n[14][0] = 1'b1;
    else
      tree_n[14][0] = 1'b0;
      
    //root.left
    if(|in[63:48])
      tree_n[13][1] = 1'b1;
    else
      tree_n[13][1] = 1'b0;
    if(|in[31:16])
      tree_n[13][0] = 1'b1;
    else
      tree_n[13][0] = 1'b0;
      
    //root.right
    if(|in[47:32])
      tree_n[12][1] = 1'b1;
    else
      tree_n[12][1] = 1'b0;
    if(|in[15:0])
      tree_n[12][0] = 1'b1;
    else
      tree_n[12][0] = 1'b0;
      
    //root.left.left
    if(|in[63:56])
      tree_n[11][1] = 1'b1;
    else
      tree_n[11][1] = 1'b0;
    if(|in[55:48])
      tree_n[11][0] = 1'b1;
    else
    tree_n[11][0] = 1'b0;
    
    //root.left.right
    if(|in[31:24])
      tree_n[10][1] = 1'b1;
    else
      tree_n[10][1] = 1'b0;
    if(|in[23:16])
      tree_n[10][0] = 1'b1;
    else
      tree_n[10][0] = 1'b0;
      
    //root.right.left
    if(|in[47:40])
      tree_n[9][1] = 1'b1;
    else
      tree_n[9][1] = 1'b0;
    if(|in[39:32])
      tree_n[9][0] = 1'b1;
    else
      tree_n[9][0] = 1'b0;
      
    //root.right.right
    if(|in[15:8])
      tree_n[8][1] = 1'b1;
    else
      tree_n[8][1] = 1'b0;
    if(|in[7:0])
      tree_n[8][0] = 1'b1;
    else 
      tree_n[8][0] = 1'b0;
      
    //root.left.left.left
    if(|in[63:60])
      tree_n[7][1] = 1'b1;
    else
      tree_n[7][1] = 1'b0;
    if(|in[59:56])
      tree_n[7][0] = 1'b1;
    else
      tree_n[7][0] = 1'b0;
      
    //root.left.left.right
    if(|in[55:52])
      tree_n[6][1] = 1'b1;
    else
      tree_n[6][1] = 1'b0;
    if(|in[51:48])
      tree_n[6][0] = 1'b1;
    else
      tree_n[6][0] = 1'b0;

    //root.left.right.left
    if(|in[31:28])
      tree_n[5][1] = 1'b1;
    else
      tree_n[5][1] = 1'b0;
    if(|in[27:24])
      tree_n[5][0] = 1'b1;
    else
      tree_n[5][0] = 1'b0;
      
    //root.left.right.right
    if(|in[23:20])
      tree_n[4][1] = 1'b1;
    else
      tree_n[4][1] = 1'b0;
    if(|in[19:16])
      tree_n[4][0] = 1'b1;
    else
      tree_n[4][0] = 1'b0;
      
    //root.right.left.left
    if(|in[47:44])
      tree_n[3][1] = 1'b1;
    else
      tree_n[3][1] = 1'b0;
    if(|in[43:40])
      tree_n[3][0] = 1'b1;
    else
      tree_n[3][0] = 1'b0;
      
    //root.right.left.right
    if(|in[39:36])
      tree_n[2][1] = 1'b1;
    else
      tree_n[2][1] = 1'b0;
    if(|in[35:32])
      tree_n[2][0] = 1'b1;
    else
      tree_n[2][0] = 1'b0;
      
    //root.right.right.left
    if(|in[15:12])
      tree_n[1][1] = 1'b1;
    else
      tree_n[1][1] = 1'b0;
    if(|in[11:8])
      tree_n[1][0] = 1'b1;
    else
      tree_n[1][0] = 1'b0;
      
    //root.right.right.right
    if(|in[7:4])
      tree_n[0][1] = 1'b1;
    else
      tree_n[0][1] = 1'b0;
    if(|in[3:0])
      tree_n[0][0] = 1'b1;
    else
      tree_n[0][0] = 1'b0;
 end
    
assign tree = tree_p;

always_ff @(posedge clk)
  begin
    if(reset)
      tree_p <= {2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00,2'b00};
    else
      tree_p <= tree_n;
  end   
            
endmodule

////root 
//assign tree_n[14][1] = ((|in[63:48]) || (|in[31:16]))? 1'b1:1'b0;   
//assign tree_n[14][0] = ((|in[47:32]) || (|in[15:0]))? 1'b1:1'b0;
////root.left
//assign tree_n[13][1] = (|in[63:48])? 1'b1:1'b0;
//assign tree_n[13][0] = (|in[31:16])? 1'b1:1'b0;
////root.right
//assign tree_n[12][1] = (|in[47:32])? 1'b1:1'b0;
//assign tree_n[12][0] = (|in[15:0])? 1'b1:1'b0;
////root.left.left
//assign tree_n[11][1] = (|in[63:56])? 1'b1:1'b0;
//assign tree_n[11][0] = (|in[55:48])? 1'b1:1'b0;
////root.left.right
//assign tree_n[10][1] = (|in[31:24])? 1'b1:1'b0;
//assign tree_n[10][0] = (|in[23:16])? 1'b1:1'b0;
////root.right.left
//assign tree_n[9][1] = (|in[47:40])? 1'b1:1'b0;
//assign tree_n[9][0] = (|in[39:32])? 1'b1:1'b0;
////root.right.right
//assign tree_n[8][1] = (|in[15:8])? 1'b1:1'b0;
//assign tree_n[8][0] = (|in[7:0])? 1'b1:1'b0;
////root.left.left.left
//assign tree_n[7][1] = (|in[63:60])? 1'b1:1'b0;
//assign tree_n[7][0] = (|in[59:56])? 1'b1:1'b0;
////root.left.left.right
//assign tree_n[6][1] = (|in[55:52])? 1'b1:1'b0;
//assign tree_n[6][0] = (|in[51:48])? 1'b1:1'b0;
////root.left.right.left
//assign tree_n[5][1] = (|in[31:28])? 1'b1:1'b0;
//assign tree_n[5][0] = (|in[27:24])? 1'b1:1'b0;
////root.left.right.right
//assign tree_n[4][1] = (|in[23:20])? 1'b1:1'b0;
//assign tree_n[4][0] = (|in[19:16])? 1'b1:1'b0;
////root.right.left.left
//assign tree_n[3][1] = (|in[47:44])? 1'b1:1'b0;
//assign tree_n[3][0] = (|in[43:40])? 1'b1:1'b0;
////root.right.left.right
//assign tree_n[2][1] = (|in[39:36])? 1'b1:1'b0;
//assign tree_n[2][0] = (|in[35:32])? 1'b1:1'b0;
////root.right.right.left
//assign tree_n[1][1] = (|in[15:12])? 1'b1:1'b0;
//assign tree_n[1][0] = (|in[11:8])? 1'b1:1'b0;
////root.right.right.right
//assign tree_n[0][1] = (|in[7:4])? 1'b1:1'b0;
//assign tree_n[0][0] = (|in[3:0])? 1'b1:1'b0;


