`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/19/2019 11:24:39 AM
// Design Name: 
// Module Name: hit_data_stream
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//serializes the encoded data into a stream. 
//stream -> level 1 , level 2 , level 3 , level 4
module hit_data_stream(
                      input logic clk,
                      input logic reset,
                      input logic [3:0] b [5:0],
                      input logic [5:0] a,
                      output logic [29:0] z,
                      output logic [4:0] length
                      );
                
integer i;
logic [4:0] index;

always_comb
  begin
    z = '0;
    index= 5'd29;
    if(a[5:4] == 0)
      begin
        z[index] = a[3];
        z[index - 5'd1] = a[2];
        z[index - 5'd2] = a[1];
        z[index - 5'd3] = a[0];
        index = index - 5'd4;
      end
    else
      begin
        z[index] = a[5];
        z[index - 5'd1] = a[4];
        z[index - 5'd2] = a[3];
        z[index - 5'd3] = a[2]; 
        z[index - 5'd4] = a[1];
        z[index - 5'd5] = a[0];
        index = index - 5'd6;
      end
              
    for(i=0;i<6;i=i+1)
      begin: final_loop
        if(b[i][3:2] != 0)
          begin
            z[index] = b[i][3];
            index = index - 5'd1;
            z[index] = b[i][2];
            index = index - 5'd1;
          end 
        if(b[i][1:0] != 0)
          begin
            z[index] = b[i][1];
            index = index - 5'd1;
            z[index] = b[i][0];
            index = index - 5'd1;
          end          
      end
    length = 5'd30 - (index + 5'd1) ;
  end  
  
//assign length = length_p;

//always_ff @(posedge clk)
//  begin
//    if(reset)
//      length <= 5'd0;
//    else
//      length <= length_n;
//  end                   
                      
endmodule
