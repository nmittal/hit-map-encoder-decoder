`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.08.2019 13:48:47
// Design Name: 
// Module Name: bit_code_replacer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//As there are only 3 used 2 - bit codes, one of them can be replaced with a 1-bit code.
//This module performs the i bit encoding which is the minimal case of Hauffman coding
// replace 01 --> 0
//Encoded stream reduced further in length

module bit_code_replacer(
                        input logic [29:0] in,
                        input logic [4:0] pre_replacer_length,
                        output logic [29:0] out,
                        output logic [4:0] post_replacer_length
                         );
                         
logic [4:0] in_index, out_index;

logic [4:0] i, j;

always_comb
  begin
    j = 0;
    out = 30'd0;
    in_index = 5'd29;
    out_index = 5'd29;
    for(i = 5'd30; i>0; i=i-2)
      begin: for_loop
        if(in[in_index] == 1'b0 & in[in_index - 1'b1] == 1'b1)
          begin
            out[out_index] = 1'b0;
            out_index = out_index - 5'd1;
            in_index = in_index - 5'd2;
            j = j + 5'd1;
          end
        else
          begin
            out[out_index] = in[in_index];
            out[out_index - 5'd1] = in[in_index - 5'd1];
            out_index = out_index - 5'd2;
            in_index = in_index - 5'd2;
          end
      end
    post_replacer_length = pre_replacer_length - j;
  end                         
                         
endmodule
