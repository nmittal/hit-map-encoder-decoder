`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2019 11:20:36
// Design Name: 
// Module Name: encoder_decoder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//To check whether the Hit Data generated by the hit generated is same as the
//output of the decoder.
//Output of the decoder is coming after 2 clock cycles.
module encoder_decoder_tb ();

logic clk;
//logic clk_i;
logic reset;
logic [77:0] in;  //random hit data 
logic [43:0] data;  //encoded data
logic [4:0] length;
logic [77:0] out;  //decoded hit data
//logic [77:0] check;

random_hit_data_generator gen(
                              .clk(clk),
                              .reset(reset),
                              .data(in)
                             );

hit_map_encoder encoder(
                       .clk(clk),
                       .reset(reset),
                       .in(in),
                       .out(data),
                       .length(length)
                       );

hit_map_decoder decoder(
                       .clk(clk),
                       .reset(reset),
                       .in(data),
                       .length(length),
                       .out(out)
                       );

//logic fifo_busy_wr;
//logic fifo_busy_rd;
//logic [77:0] fifo_out;
//logic wr_en, rd_en;

//fifo_generator_0 fifo(
//                      .wr_clk(clk),
//                      .rd_clk(clk_i),
//                      .rst(reset),
//                      .wr_rst_busy(fifo_busy_wr),
//                      .rd_rst_busy(fifo_busy_rd),
//                      .full(),
//                      .empty(),
//                      .wr_en(wr_en),
//                      .rd_en(rd_en),
//                      .din(in),
//                      .dout(fifo_out)
//                      );
 
           
parameter ClockDelayWrite = 6.25;  //160MHz
//parameter ClockDelayRead = 25.0; //40MHz
                         
initial
 begin
  clk <= 1;
  forever #(ClockDelayWrite/2) clk <= ~clk;
 end

//initial
// begin
//  clk_i <= 1;
//  forever #(ClockDelayRead/2) clk_i <= ~clk_i;
// end

//integer error, run_i;

//task doRun();
//  @(posedge clk)
//  if(fifo_out != out)
//    error++;
  
//endtask
integer error;

initial
  begin
    reset <= 1'b1;
//    wr_en = 1'b0;
//    rd_en = 1'b0;
    error = 0;
    @(posedge clk);
    @(posedge clk);
    reset <= 1'b0; 
//    @(posedge clk)
//    @(posedge clk)
//    for(run_i = 0; run_i <20; run_i = run_i + 1)
//      begin: entries
//        doRun();
//      end
//    $display("number of mismatches: %d", error);
  end       


logic [77:0] temp1, temp2;

always @(posedge clk)
  begin
    if(reset)
     temp1 <= 0;
    else
     temp1 <= in;
  end

always @(posedge clk)
  begin
    if(reset)
      temp2 <= 0;
    else
      temp2 <= temp1;
  end

always @(*)
  begin
    if(temp2 != out)
      error = error +1;
  end
  
//always @(posedge clk)
//  begin
//   if(fifo_busy_wr != 1'b1)
//     wr_en = 1'b1;
//   else
//     wr_en = 1'b0;
//   if(fifo_busy_rd != 1'b1)
//     rd_en = 1'b1;
//   else
//     rd_en = 1'b0;
//  end
           
 initial
   begin
     if(error <= 1)
     $display("NO MISMATCH FOUND");
     else
     $display("NUMBER OF MISMATCH: %d", error);
   end          
                
endmodule
