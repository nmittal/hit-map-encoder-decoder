`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.08.2019 14:44:53
// Design Name: 
// Module Name: random_hit_data_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module random_hit_data_generator(
                                 input logic clk,
                                 input logic reset,
                                 output logic [77:0] data
                                 );
                                 
logic [31:0] randout;

integer i;
logic [5:0] pos;

simplePRNG random_number(
                        .clk(clk),
                        .reset(reset), 
                        .load(1'b0), 
                        .seed(32'hF00DFACE),
                        .step(1'b1),
                        .dout(randout), 
                        .new_data()
                        );
                                 
always_comb
  begin
    data[77:64] = randout[29:16];
    pos = 6'd0;
    for(i=0; i<16; i=i+1)
      begin: hit_data
        if(randout[i])
          begin
            data[pos] = 1'b1;
            data[pos+6'b1] = 1'b1;
            data[pos+6'd2] = 1'b1;
            data[pos+6'd3] = 1'b1;
          end
        else
          begin
            data[pos] = 1'b0;
            data[pos+6'b1] = 1'b0;
            data[pos+6'd2] = 1'b0;
            data[pos+6'd3] = 1'b0;            
          end
        pos = pos + 6'd4;
      end
  end                             
endmodule


module random_hit_tb();
logic clk;
logic reset;
logic [77:0] data;

random_hit_data_generator gen(.*);

initial
  begin
    clk = 1'b0;
  end

always #5 clk=~clk;

initial
  begin
    @(posedge clk);
    reset = 1'b1;
    @(posedge clk);
    reset = 1'b0;
    @(posedge clk);
  end

endmodule