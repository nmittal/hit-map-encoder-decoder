`timescale 1ns / 1ps

module simplePRNG  #(parameter width = 32) (clk, reset, load, seed, step, dout, new_data);
    input clk, reset, load, step;
    input [31:0] seed;
    output [31:0] dout;
    output reg new_data;
    
    //some logic
    reg [31:0] lfsr; 
    
    //check widths
    //assert ((width >= 3) && (width <= 32)) else $error ("width must be between 3 and 32!");
    
    //main piece
    always @(posedge clk) begin
        if (reset || load) begin
            lfsr[31:0] <= seed[31:0];
            new_data <= 1'b0;
        end else if (step) begin
            new_data <= 1'b1;
            //set most values
            lfsr[23:0] <= lfsr[24:1];
            lfsr[24] <= lfsr[25] ^ lfsr[0];
            lfsr[25] <= lfsr[26] ^ lfsr[0];
            lfsr[28:26] <= lfsr[29:27];
            lfsr[29] <= lfsr[30] ^ lfsr[0];
            lfsr[30] <= lfsr[31];
            lfsr[31] <= lfsr[0];
        end else begin
            lfsr [31:0] <= lfsr[31:0];
            new_data <= 1'b0;
        end
    end
    //assign dout 
    assign dout = lfsr;
endmodule
