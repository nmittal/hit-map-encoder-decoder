`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2019 13:07:58
// Design Name: 
// Module Name: binary_stream_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//      |node[7][1]|node[7][0]|node[6][1]|node[6][0]|node[3][1]|node[3][0]|node[2][1]|node[2][0]|
//      |node[5][1]|node[5][0]|node[4][1]|node[4][0]|node[1][1]|node[1][0]|node[0][1]|node[0][0]|
// This node bits will tell the data in the corresponding position in the Hit Data 
//      |63:60|59:56|55:52|51:48|47:44|43:40|39:36|35:32|
//      |31:28|27:24|23:20|19:16|15:12|11:8 |7:4  |3:0  |
//de-serializes the decoded data into a stream. 


module binary_stream_decoder(
                             input logic clk,
                             input logic reset,
                             input logic [1:0] tree [14:0],
                             output logic [63:0] hit
                             );
                             
logic [63:0] hit_n, hit_p;
logic [1:0] tree_temp [ 14:0];
integer i,j;

logic [1:0] temp1, temp2;
logic [5:0] pos;

always_comb
  begin
    tree_temp = tree;
    temp1 = tree[3];   //swapping the values for a parallelization
    tree_temp[3] = tree_temp[5];
    tree_temp[5] = temp1;
                          
    temp2 = tree[2];          
    tree_temp[2] = tree_temp[4];
    tree_temp[4] = temp2;
    
    pos = 6'd63;
    //
    for(i=7; i>=0; i=i-1)
      begin: leaf_node_traversal
        for(j=1; j>=0; j=j-1)
          begin: node_bit_value
            if(tree_temp[i][j])
              begin
                hit_n[pos] = 1'b1;
                hit_n[pos-6'd1] = 1'b1;
                hit_n[pos-6'd2] = 1'b1;
                hit_n[pos-6'd3] = 1'b1;
                pos = pos - 6'd4;
              end
            else
              begin
                hit_n[pos] = 1'b0;
                hit_n[pos-6'd1] = 1'b0;
                hit_n[pos-6'd2] = 1'b0;
                hit_n[pos-6'd3] = 1'b0;
                pos = pos - 6'd4;
               end
          end
    end
  end
  
    always_ff @(posedge clk)
    begin
      if(reset) 
        begin
          hit_p <= 64'd0;
        end
      else
        begin
          hit_p <= hit_n;
        end
   end  
  
assign hit = hit_p;

endmodule
