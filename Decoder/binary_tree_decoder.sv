`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2019 13:00:16
// Design Name: 
// Module Name: binary_tree_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//                       TREE STRUCTURE                     
//                             14                      <- LEVEL 1  (root)
//                       /           \
//                     13             12               <- LEVEL 2
//                   /    \          /   \
//                 11      10      9      8            <- LEVEL 3
//               /   \    /  \   /  \   /  \
//              7    6   5   4  3    2  1   0          <- LEVEL 4
//
//This is the addressing of the tree used in the code below. 
//Each node is 2 bits each.    
//If the MSB = 1 --> There exist a left child of that node
//else left child == 2'b00
//same goes with LSB, which is for right child of that node.

//Hit data is arranged in a 2x8 block
//             63:32
//             31:0       Height = 2 pixels        Width = 8 pixels   [1 pixel = 4bits] 

module binary_tree_decoder(
                           input logic [29:0] bit_data,
                           output logic [1:0] tree [14:0]
                           );
    
logic [29:0] data;   
logic [3:0] node, nextNode;  //tree node
logic [4:0] index;  //data indexing

integer i,j;
    
always_comb    
  begin
    data = bit_data;
    node = 4'd14;    //new node (child)
    nextNode = 4'd14;  //parent node
    index = 5'd29;  
    //root is always the first 2 bits of the encoded data
    tree[node][1] = data[index];
    index = index - 5'd1;
    tree[node][0] = data[index];
    index = index - 5'd1;
    node = node - 4'd1;
    //this for loop will check if the parent node bits are 1 or 0.
    //depending upon that, it will give the values to the nodes.
    for(i = 7; i>0; i=i-1)
      begin: node_loop
        for(j=1; j>=0; j=j-1)
          begin: node_bit_loop
            if(tree[nextNode][j])
              begin
                tree[node][1] = data[index];
                index = index - 5'd1;
                tree[node][0] = data[index];
                index = index - 5'd1;
                node = node - 4'd1;
              end
            else
              begin
                tree[node] = 2'b00;
                node = node - 4'd1;
              end
          end
          nextNode = nextNode - 4'd1;
      end   
   end
   
endmodule
